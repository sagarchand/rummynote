import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ScoreTableService} from '../service/scoreTable.service';
import { Round} from '../model/round';
import { PlayerScore} from '../model/playerscore';
import { SocketService } from '../service/socket.service';
import { GameRoom } from '../model/gameroom';
import { Event } from '../model/event';

@Component({
  selector: 'score-table',
  templateUrl: './scoreTable.component.html',
  styleUrls: ['./scoreTable.component.css']
})

export class ScoreTableComponent implements OnInit {
  title = 'ScoreTable';
  ioConnection: any;

  constructor(public scoreTableService:ScoreTableService,
  	public dialog: MatDialog,
  	private socketService: SocketService
  	) {
  	
  }

  ngOnInit(): void {
  	this.scoreTableService.gameRoomSubject.subscribe((room:GameRoom)=>{
  		this.socketService.send(room);
  	});
    this.initModel();
    this.initIoConnection();
  }

  showNewScoresPopup():void {
  	let dialogRef =  this.dialog.open(NewScorePopup);
  }

  editRoundScore(round) {
	let dialogRef =  this.dialog.open(EditScorePopup, {
		data: round
	});
  }

  private initModel(): void {
    
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.ioConnection = this.socketService.onMessage()
      .subscribe((gameRoom: GameRoom) => {
        this.scoreTableService.gameRoom = gameRoom;
      });


    this.socketService.onEvent(Event.CONNECT)
      .subscribe(() => {
        console.log('connected');
      });

    this.socketService.onEvent(Event.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected');
      });
  }
  
}

@Component ({
	selector: 'new-score-popup',
	templateUrl: './add-score-popup.html'
})

export class NewScorePopup implements OnInit {
	
	public myRoundScores = [];

	constructor(
	    public dialogRef: MatDialogRef<NewScorePopup>,
	    @Inject(MAT_DIALOG_DATA) public data: any,
	    public scoreTableService: ScoreTableService) {
			this.scoreTableService.gameRoom.players.forEach(player => {
				this.myRoundScores.push({
					playerName: player.playerName,
					playerScore: null,
					playerStatus: player.playerStatus
				})
			});
	    }

	ngOnInit() {
		console.log(this.myRoundScores);
	}

	  onNoClick(): void {
	    this.dialogRef.close();
	  }

	addNewScore():void {
		this.scoreTableService.addRound(this.myRoundScores);
	}
}

@Component ({
	selector: 'edit-score-popup',
	templateUrl: './edit-score-popup.html'
})

export class EditScorePopup implements OnInit {
	public editRound: Round;

	constructor(
	    public dialogRef: MatDialogRef<EditScorePopup>,
	    @Inject(MAT_DIALOG_DATA) public data: any,
	    public scoreTableService: ScoreTableService) {
			this.editRound = data;
			console.log(this.editRound);
	    }

	ngOnInit() {
		
	}

	onNoClick(): void {
	    this.dialogRef.close();
	}

	saveScore():void {
		this.scoreTableService.saveRound(this.editRound.id, this.editRound);
	}
}
