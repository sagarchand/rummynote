import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ScoreTableService } from '../../service/scoreTable.service';

@Component({
  selector: 'app-confirm-new-game-dialog',
  templateUrl: './confirm-new-game-dialog.component.html',
  styleUrls: ['./confirm-new-game-dialog.component.css']
})
export class ConfirmNewGameDialogComponent implements OnInit {
  constructor(private router:Router,
    private scoreTableService:ScoreTableService) { }

  ngOnInit() {
  }

  startNewGame() {
    this.scoreTableService.startNewGame();
    this.router.navigate(['']);
  }
}
