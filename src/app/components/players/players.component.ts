import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Player } from '../../model/player';
import { ScoreTableService } from '../../service/scoreTable.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Component({
	'selector':'players',
	'templateUrl': './players.component.html',
  	styleUrls: ['./players.component.css']
})

export class PlayersComponent implements OnInit {
	public players: Player[];

	constructor(public dialog: MatDialog, public scoreTableService: ScoreTableService) {
		this.players = scoreTableService.gameRoom.players;
	}

	showNewPlayerPopup(): void {
		let dialogRef =  this.dialog.open(AddPlayerPopup, {
			data: {
				players: this.players
			}
		});
		dialogRef.afterClosed().subscribe(result => {
	      console.log('The dialog was closed');
	    });
	}

	ngOnInit() {
		if(this.players.length == 0) {
			this.showNewPlayerPopup();
		}
    }

}

@Component({
  selector: 'add-player-popup',
  templateUrl: './add-player-popup.html',
})
export class AddPlayerPopup {
	players:Player[];

  constructor(
    public dialogRef: MatDialogRef<AddPlayerPopup>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private scoreTableService: ScoreTableService) { 
  	this.players = data.players;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addNewPlayer(playerName:String): void {
  	playerName = playerName.trim();
	this.scoreTableService.addPlayer({playerName: playerName, playerStatus: true} as Player);
	
  }

}