import { Component, OnInit, Inject, Input } from '@angular/core';
import {ScoreTableService} from '../../service/scoreTable.service';
import {Settings} from '../../model/settings';
import {Router} from "@angular/router";

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})

export class SettingsComponent {
	public settings: Settings;

	constructor(private scoreTableService:ScoreTableService, private router: Router) {
  		this.settings = scoreTableService.gameRoom.settings;
  	}

  	saveSettings() {
  		this.router.navigate(['']);
  	}
}