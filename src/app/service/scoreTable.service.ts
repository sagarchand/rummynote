import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { Player} from '../model/player';
import { PlayerScore} from '../model/playerscore';
import { Round} from '../model/round';
import { Settings} from '../model/settings';
import { GameRoom } from '../model/gameroom';
import { Subject } from 'rxjs/Subject';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ScoreTableService {
 // public players = [];
 // public rounds = [];
 // public roundCount: number = 1;
 // public settings: Settings = new Settings();
 gameRoom: GameRoom = new GameRoom();
 currentRoomId: number;
 public gameRoomSubject = new Subject();

 constructor() {
 	//this.players.push({playerName:"sagar",playerInitialScore:0,playerTotalScore:0},{playerName:'chand',playerInitialScore:0,playerTotalScore:0});
 }

 addPlayer(player: Player):void {
 	this.gameRoom.players.push(player);
 	if(!this.gameRoom.gameStarted)
 		this.gameRoom.gameStarted = true;
 	this.gameRoomSubject.next(this.gameRoom);
 }

 updatePlayerScores():void {
 	console.log(this);
 	var playerTotal = [];

 	this.gameRoom.players.forEach(player => {
 		playerTotal[''+player.playerName] = 0;
 	});
 	this.gameRoom.rounds.forEach(round => {
 		round.scores.forEach(score => {
 			playerTotal[''+score.playerName] += score.playerScore;
 		})
 	})
 	this.gameRoom.players.forEach(player=>{
 		player.playerTotalScore = playerTotal[''+player.playerName];
 		player.playerRemainingDrops = Math.floor((this.gameRoom.settings.maxGameScore - player.playerTotalScore) / this.gameRoom.settings.dropScore);
 		if(player.playerRemainingDrops < 0) player.playerRemainingDrops = 0;
 		if(player.playerTotalScore > this.gameRoom.settings.maxGameScore) {
 			player.playerStatus = false;
 		} else {
 			player.playerStatus = true;
 		}
 	})
 	this.gameRoomSubject.next(this.gameRoom);
 }

 addRound(scores: PlayerScore[]) {
 	let round = new Round();
 	round.id = this.gameRoom.roundCount;
 	round.scores = scores;
 	this.gameRoom.rounds.push(round);
 	this.gameRoom.roundCount++;
 	this.updatePlayerScores();
 }

 saveRound(roundId:number, round: Round) {
 	this.gameRoom.rounds.map((round, i) => {
 		if(round.id == roundId) {
 			this.gameRoom.rounds[i] = round;
 		}
 	})
 	this.updatePlayerScores();
 }

 startNewGame() {
	 this.gameRoom = new GameRoom;
 }

}