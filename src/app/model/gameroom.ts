import { Player } from './player';
import { Settings } from './settings';
import { Round } from './round';

export class GameRoom {
 roomId: number = 0;
 roomName: string;
 players: Player[];
 rounds: Round[];
 roundCount: number;
 settings: Settings;
 gameStarted: boolean = false;

 constructor() {
 	this.roomId = this.roomId + 1;
 	this.roomName = "";
 	this.players = [];
 	this.rounds = [];
 	this.roundCount = 1;
 	this.settings =  new Settings();
 }

}
