export class Settings {
	maxGameScore: number;
	dropScore: number;

	constructor() {
		this.maxGameScore = 100;
		this.dropScore = 25;
	}
}