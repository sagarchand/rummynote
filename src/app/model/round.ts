import {PlayerScore} from './playerscore';

export class Round {
	id: number;
	scores: PlayerScore[];
};