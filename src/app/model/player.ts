export class Player {
	playerInitialScore: number = 0;
	playerName: String;
	playerTotalScore: number = 0;
	playerStatus: boolean = true;
	playerRemainingDrops: number;

	constructor(playerName) {
		this.playerName = playerName;
		this.playerInitialScore = 0;
		this.playerTotalScore = 0;
		this.playerStatus = true;
	}
}