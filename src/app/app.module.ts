import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { provideRoutes } from '@angular/router';
import { MaterialModule } from './shared/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {MatFormFieldModule } from '@angular/material/form-field';

import { AppComponent } from './app.component';
import { ScoreTableComponent } from './components/scoreTable.component';
import { RoomMenu } from './components/menu/room-menu.component';
import { PlayersComponent } from './components/players/players.component';
import { AddPlayerPopup } from './components/players/players.component';
import { NewScorePopup,EditScorePopup } from './components/scoreTable.component';
import { AppRoutingModule } from './app-routing.module';

import {ScoreTableService} from './service/scoreTable.service';
import {PlayerScore} from './model/playerscore';
import { SettingsComponent } from './components/settings/settings.component';
import { SocketService } from './service/socket.service';
import { ConfirmNewGameDialogComponent } from './components/confirm-new-game-dialog/confirm-new-game-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    ScoreTableComponent,
    PlayersComponent,
    AddPlayerPopup,
    NewScorePopup,
    EditScorePopup,
    SettingsComponent,
    RoomMenu,
    ConfirmNewGameDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [  	
    ScoreTableService,
    PlayerScore,
    SocketService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ AddPlayerPopup, NewScorePopup, EditScorePopup, ConfirmNewGameDialogComponent ]
})
export class AppModule { }
