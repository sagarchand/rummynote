import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScoreTableComponent }   from './components/scoreTable.component';
import { PlayersComponent }      from './components/players/players.component';
import { SettingsComponent } from './components/settings/settings.component';


const routes: Routes = [
  { path: '', redirectTo: '/scoreboard', pathMatch: 'full' },
  { path: 'scoreboard', component: ScoreTableComponent },
  { path: 'players', component: PlayersComponent },
  { path: 'settings', component: SettingsComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/