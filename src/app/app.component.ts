import { Component } from '@angular/core';
import { ConfirmNewGameDialogComponent } from './components/confirm-new-game-dialog/confirm-new-game-dialog.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ScoreTableService } from './service/scoreTable.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private scoreTableService: ScoreTableService,
              public dialog: MatDialog) {

  }

  confirmNewGame() {
    let dialogRef =  this.dialog.open(ConfirmNewGameDialogComponent);
  }
}
