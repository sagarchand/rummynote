# Rummynote

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

This is an App to note down the Score of players when playing the Indian Card game Rummy. The main idea is to explore the functionality of the Angular 2 framework and to see how far it can be used and the correct way to be used. I am using this app as a playground to learn Angular, so please feel free to change stuff and move around things to improve them. Much appreciated.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
